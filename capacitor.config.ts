import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'fr.abcduplombier44.fr.app',
  appName: 'ABC du Plombier',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
