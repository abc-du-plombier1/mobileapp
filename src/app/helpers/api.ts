import {Account, Client, Databases, Storage} from 'appwrite';
import {environment} from '../../environments/environment';

export class Api {
  private static sdk: Client | null;
  private static account: Account | null;
  private static storage: Storage | null;
  private static database: Databases | null;

  static provider() {
    if (this.sdk) {return this.sdk;}
    const client = new Client();
    client
      .setEndpoint(environment.serverEndpoint)
      .setProject(environment.projectId)
      .setLocale('fr-FR');
    this.sdk = client;
    return this.sdk;
  }

  static databases(): Databases {
    if (!this.sdk) {
      this.provider();
    }
    if (this.database) {
      return this.database;
    }

    this.database = new Databases(this.sdk, 'default');
    return this.database;
  }

  static accounts(): Account {
    if (!this.sdk) {
      this.provider();
    }
    if (this.account) {
      return this.account;
    }

    this.account = new Account(this.sdk);
    return this.account;
  }

  static storages(): Storage {
    if (!this.sdk) {
      this.provider();
    }
    if (this.storage) {
      return this.storage;
    }

    this.storage = new Storage(this.sdk);
    return this.storage;
  }
}
