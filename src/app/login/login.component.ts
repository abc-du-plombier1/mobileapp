import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from './store/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  @ViewChild('email') email!: ElementRef;
  @ViewChild('password') password!: ElementRef;

  error: string;
  constructor(
    private router: Router,
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      this.error = authState.authError;
    });
  }

  onLogin() {
    console.log(this.email.nativeElement.value);
    this.store.dispatch(new AuthActions.LoginStart({
      email: this.email.nativeElement.value,
      password: this.password.nativeElement.value
    }));
  }
}
