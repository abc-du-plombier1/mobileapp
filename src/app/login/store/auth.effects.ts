import {Actions, Effect, ofType} from '@ngrx/effects';
import {switchMap, tap} from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import {Injectable} from '@angular/core';
import { Api } from '../../helpers/api';
import {Router} from '@angular/router';
import {User} from '../user.model';

@Injectable()
export class AuthEffects {

  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authData: AuthActions.LoginStart) => Api
      .accounts().createEmailSession(authData.payload.email, authData.payload.password)
      .then((response) => {
        const expirationDate = new Date((new Date().getTime() + +response.providerAccessTokenExpiry * 1000));
        console.log('Token', response);
        const user = new User(response.providerUid, response.userId, response.providerAccessToken, expirationDate);
        localStorage.setItem('userData', JSON.stringify(user));
        return new AuthActions.AuthenticatedSuccess({
            providerUid: response.providerUid,
            userId: response.userId,
            token: response.providerAccessToken,
            expirationDate
          });
      }, (error) => {
        console.log('Error ', error);
        let errorMessage = 'Une erreur c\'est produite';

        switch (error.message) {
          case 'Invalid credentials':
            errorMessage = 'Mot de passe invalide';
            break;
          case 'general_rate_limit_exceeded':
            errorMessage = 'Limite de connexion atteinte';
            break;
        }
        return new AuthActions.AuthenticatedFail(errorMessage);
    }))
  );

  @Effect()
  autoLogin = this.actions$.pipe(
    ofType(AuthActions.AUTO_LOGIN),
    switchMap(() => Api.accounts().getSessions().then(
        res => {
          console.log('RES', res);
          if (!res || !res.sessions[0].userId) {
            new AuthActions.Logout();
          }
          const user = res.sessions[0];
          const expirationDate = new Date((new Date().getTime() + +user.providerAccessTokenExpiry * 1000));
          console.log(
            {
              providerUid: user.providerUid,
              userId: user.userId,
              token: user.providerAccessToken,
              expirationDate
            }
          );
          // return {type: 'DUMMY'};
          return new AuthActions.AuthenticatedSuccess({
            providerUid: user.providerUid,
            userId: user.userId,
            token: user.providerAccessToken,
            expirationDate
          });
      },
        () => ({ type: 'DUMMY' })))
  );

  @Effect({dispatch: false})
  authRedirect = this.actions$.pipe(ofType(AuthActions.AUTHENTICATED_SUCCESS ), tap(() => {
    this.router.navigate(['/Photos/photos']);
  }));

  @Effect({dispatch: false})
  authLogout = this.actions$.pipe(
    ofType(AuthActions.LOGOUT),
    tap(() => {
      Api.accounts().deleteSessions().then();
      this.router.navigate(['/login']);
    })
  );
  constructor(private actions$: Actions, private router: Router) {
  }
}
