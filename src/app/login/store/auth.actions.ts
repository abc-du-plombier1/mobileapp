import {Action} from '@ngrx/store';

export const LOGIN_START = '[Auth] Login Start';
export const AUTHENTICATED_SUCCESS = '[Auth] Login';
export const AUTHENTICATED_FAIL = '[Auth] Login Fail';
export const LOGOUT = '[Auth] Logout';
export const AUTO_LOGIN = '[Auth] Auto Login';

export class AuthenticatedSuccess implements Action {
  readonly type = AUTHENTICATED_SUCCESS;

  constructor(
    public payload: {
      providerUid: string;
      userId: string;
      token: string;
      expirationDate: Date;
    }) {
  }
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class LoginStart implements  Action {
  readonly type = LOGIN_START;

  constructor(public payload: {email: string; password: string}) {
  }
}

export class AuthenticatedFail implements Action {
  readonly type = AUTHENTICATED_FAIL;

  constructor(public payload: string) {
  }
}

export class AutoLogin implements Action {
  readonly type = AUTO_LOGIN;
}

export type AuthActions =
  AuthenticatedSuccess  |
  LoginStart            |
  AuthenticatedFail     |
  Logout                |
  AutoLogin;
