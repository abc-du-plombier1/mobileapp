import {StoreModule} from '@ngrx/store';
import { NgModule } from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import * as fromApp from './store/app.reducer';
import {ReactiveFormsModule} from '@angular/forms';
import { RouteReuseStrategy } from '@angular/router';
import {AuthEffects} from './login/store/auth.effects';
import { AppRoutingModule } from './app-routing.module';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([AuthEffects]),
    StoreDevtoolsModule.instrument({ logOnly: environment.production }),
    ReactiveFormsModule
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
