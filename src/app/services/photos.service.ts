import {Injectable} from '@angular/core';
import { Api } from '../helpers/api';
import {from, Observable} from 'rxjs';
import {PhotosModel} from '../shared/photos.model';


@Injectable({providedIn: 'root'})
export class PhotosService {

  bucketID = '62b8b55ed810d77117de';
  collectionID = '62b8b64047898200d4ed';
  listFiles: any = [];
  photosObject: PhotosModel[] = [];


  constructor() {
  }

  uploadPhoto(image: File, description: string) {
    console.log(description);
    return from(Api.storages().createFile(this.bucketID, 'unique()', image, ['role:all']).then(res => {
      console.log(res);
      if(res.$id) {
        Api.databases().createDocument(
          this.collectionID,
          res.$id,
          {
            description
          }
        );
      }
    }));
  }

  listAllStorageFiles(): Observable<object> {
    // return from(Api.provider().storage.listFiles(this.bucketID));
    return from([]);
  }

  getPhotos(): PhotosModel[] {
    this.photosObject = [];
    this.listFiles = [];
    Api.storages().listFiles(this.bucketID).then(
      res => {
        this.listFiles.push(...res.files);
        if (this.listFiles.length < 1) {
          return;
        }
        for (const photo of this.listFiles) { // Loop throw photos
          const result = Api.storages().getFileView(this.bucketID, photo.$id);
          Api.databases().getDocument(this.collectionID, photo.$id).then((response: any) => { // Get comment associate to the photo
            this.photosObject.push(new PhotosModel(photo.$id, result.href, response.description));
          });
        }
      }
    );
    console.log(this.photosObject);
    return this.photosObject;
  }

  deletePhoto(id: string) {
    return Api.storages().deleteFile(this.bucketID, id).then(() => {
      Api.databases().deleteDocument(this.collectionID, id);
      return 'ok';
    });
  }

}
