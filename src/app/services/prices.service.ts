import {Injectable} from '@angular/core';
import {Api} from '../helpers/api';
import {Query} from 'appwrite';

@Injectable({providedIn: 'root'})
export class PricesService {

  collectionID = '62b8b4eb3874ceb6e158';

  addPrice(name: string, price: number) {
    const promise = Api.databases().createDocument(this.collectionID, 'unique()', {
      name,
      price
    }, ['role:all']);
  }

  getActions() {
    return Api.databases().listDocuments(this.collectionID);
  }

  getAction(id: string) {
    return Api.databases().listDocuments(this.collectionID,
      [Query.equal('$id', id)]);
  }

  updatePrice(id: string, name: string, price: number) {
    return Api.databases().updateDocument(this.collectionID, id, {
      name,
      price
    });
  }

  deletePrice(id) {
    return Api.databases().deleteDocument(this.collectionID, id);
  }
}
