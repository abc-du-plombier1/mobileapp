import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Camera, CameraResultType, CameraSource, Photo} from '@capacitor/camera';
import {ModalController} from '@ionic/angular';
import {PhotoModalComponent} from './photo/photo-modal/photo-modal.component';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  folder: string;

  constructor(private activatedRoute: ActivatedRoute,
              public modalController: ModalController) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

}
