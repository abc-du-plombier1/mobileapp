import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';
import {PhotoModalComponent} from './photo/photo-modal/photo-modal.component';
import {PhotoComponent} from './photo/photo.component';
import {PricesComponent} from './prices/prices.component';
import {PricesModalComponent} from './prices/prices-modal/prices-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule
  ],
  declarations: [FolderPage, PhotoModalComponent, PhotoComponent, PricesComponent, PricesModalComponent]
})
export class FolderPageModule {}
