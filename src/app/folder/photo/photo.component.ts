import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Camera, CameraResultType, CameraSource} from '@capacitor/camera';
import {PhotoModalComponent} from './photo-modal/photo-modal.component';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {PhotosModel} from '../../shared/photos.model';
import {PhotosService} from '../../services/photos.service';
import {timeout} from 'rxjs/operators';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss'],
})
export class PhotoComponent implements OnInit {
  photos: PhotosModel[] = [];

  constructor(public modalController: ModalController,
              private activatedRoute: ActivatedRoute,
              private photoService: PhotosService,
              public actionSheetController: ActionSheetController) { }

  ngOnInit() {
    this.activatedRoute.snapshot.paramMap.get('id');
    this.photos = this.photoService.getPhotos();
  }

  async onAddImage() {
    const modal = await this.modalController.create({
      component: PhotoModalComponent,
    });
    modal.onDidDismiss().then(() => {
      setTimeout(() => {
        this.photos = this.photoService.getPhotos();
      }, 1000);
    });
    return await modal.present();
  }

  deleteImage(id: string) {
    this.photoService.deletePhoto(id).then(res => {
      console.log(res);
      for (let i=0; i<this.photos.length; i++) {
        if (this.photos[i].id === id) {
          this.photos.splice(i);
        }
      }
    });
  }

  async onPhotoSelected(id: string) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Que voulez-vous faire ?',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Supprimer',
        role: 'destructive',
        icon: 'trash',
        id: 'delete-button',
        data: {
          type: 'delete'
        },
        handler: () => {
          this.deleteImage(id);
        }
      },
        {
        text: 'Annuler',
        icon: 'close',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }
}
