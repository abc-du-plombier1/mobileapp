import {PhotosService} from '../../../services/photos.service';
import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {concatMap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {PhotosModel} from '../../../shared/photos.model';

@Component({
  selector: 'app-photo-modal',
  templateUrl: './photo-modal.component.html',
  styleUrls: ['./photo-modal.component.scss'],
})
export class PhotoModalComponent implements OnInit {
  fileToSend: File;
  photoComment: string;
  public files: [] = [];

  constructor(public modalController: ModalController,
              private photoService: PhotosService) {
  }

  ngOnInit() {
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  onFileSelected(event) {
    if (!event.target && !event.target.files[0]) {
      return;
    }
    this.fileToSend = event.target.files[0];
    const preview = URL.createObjectURL(this.fileToSend);
    document.getElementById('imagePreview').setAttribute('src', preview);
  }

  async onSubmit() {
    if (!this.fileToSend) {
      return;
    }
    this.photoService
      .uploadPhoto(this.fileToSend, this.photoComment)
      .pipe(concatMap(() => this.loadFiles()))
      .subscribe((files: any) => this.files = files.files);
    this.dismissModal();
  }

  private loadFiles(): Observable<object> {
    return this.photoService.listAllStorageFiles();
  }
}
