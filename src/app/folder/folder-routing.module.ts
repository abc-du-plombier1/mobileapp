import {PricesComponent} from './prices/prices.component';
import { Routes, RouterModule } from '@angular/router';
import {PhotoComponent} from './photo/photo.component';
import { FolderPage } from './folder.page';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: ':id', component: FolderPage, children: [
      { path: 'photos', component: PhotoComponent },
      { path: 'prices', component: PricesComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FolderPageRoutingModule {}
