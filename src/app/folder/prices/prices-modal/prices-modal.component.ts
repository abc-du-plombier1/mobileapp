import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {PricesService} from '../../../services/prices.service';

@Component({
  selector: 'app-prices-modal',
  templateUrl: './prices-modal.component.html',
  styleUrls: ['./prices-modal.component.scss'],
})
export class PricesModalComponent implements OnInit {
  @Input() name: string;
  @Input() price: number;
  @Input() editMode = false;
  @Input() id: string;

  constructor(private modalController: ModalController,
              private priceService: PricesService) { }

  dismissModal() {
    this.editMode = false;
    this.modalController.dismiss();
  }

  ngOnInit() {}

  onSubmit() {
    console.log(this.name);
    if (!this.editMode) {
      this.priceService.addPrice(this.name, this.price);
    }
    else {
      this.priceService.updatePrice(this.id, this.name, this.price);
    }
    this.modalController.dismiss();
  }

  onDelete() {
    this.priceService.deletePrice(this.id);
    this.editMode = false;
    this.modalController.dismiss();
  }

}
