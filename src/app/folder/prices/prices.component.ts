import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {PricesModalComponent} from './prices-modal/prices-modal.component';
import {PricesService} from '../../services/prices.service';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss'],
})
export class PricesComponent implements OnInit {

  prices: any[] = [];

  constructor(public modalController: ModalController,
              private priceService: PricesService) { }

  ngOnInit() {
    this.fetchPrice();
  }

  fetchPrice() {
    this.prices = [];
    this.priceService.getActions().then((res: any) => {
      if (res.documents) {
        this.prices.push(...res.documents);
        console.log(this.prices);
      }
    }, err => {
      console.log(err);
    });
  }

  async onAddService() {
    const modal = await this.modalController.create({
      component: PricesModalComponent,
    });
    modal.onDidDismiss().then(() => {
      this.fetchPrice();
    });
    return await modal.present();
  }

  doRefresh(event) {
    this.fetchPrice();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onClick(id: string) {
    console.log(id);
    this.priceService.getAction(id).then(async (res: any) => {
      console.log(res);
      const modal = await this.modalController.create({
        component: PricesModalComponent,
        componentProps: {
          name: String(res.documents[0].name),
          price: res.documents[0].price,
          editMode: true,
          id
        }
      });
      modal.onDidDismiss().then(() => {
        this.fetchPrice();
      });
      return await modal.present();
    }, err => {
      console.log(err);
    });
  }
}
