import * as AuthActions from './login/store/auth.actions';
import {Component, OnInit} from '@angular/core';
import * as fromApp from './store/app.reducer';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit{
  public appPages = [
    { title: 'Photo', url: '/Photos/photos', icon: 'image' },
    { title: 'Prix', url: '/Prix/prices', icon: 'cash' },
    // { title: 'Paramètre', url: '/folder/Paramètre', icon: 'settings' }
  ];
  isConnected = false;
  constructor(private store: Store<fromApp.AppState>,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      this.isConnected = !!authState.user;
      console.log(authState.user);
    });
    console.log(this.activatedRoute.url);
    this.store.dispatch(new AuthActions.AutoLogin());
  }

  onLogout() {
    this.store.dispatch(new AuthActions.Logout());
  }
}
